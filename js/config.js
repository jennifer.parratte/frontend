function config() {
    //creation de la section pour le choix du model de véicule
    var config = document.createElement('section');
        config.id = 'config';
    
    //creation du carousel
    var carousel1 = document.createElement('div');
        carousel1.className = 'glide';
        carousel1.id = 'carousel1';

    var visualisation = document.createElement('div');
        visualisation.className = 'glide_track';
        visualisation.setAttribute('data-glide-el', 'track');
        visualisation.id = 'visualisation';

    var choixVisualisation = document.createElement('ul');
        choixVisualisation.className = 'glide_slides';
        choixVisualisation.id = 'choixVisualisation';

    //creation de la div pour le choix du model pure
    var modelPure = document.createElement('li');
        modelPure.id = 'modelPure';
        modelPure.className = 'glide_slide';

    //creation de l'image du model pure
    var imgModelPure = document.createElement('img');
        imgModelPure.id = 'imgModelPure';
        imgModelPure.src = './assets/configurateur/modele/selection/pure.png';

    //creation de la div pour le choix du model legende
    var modelLegende = document.createElement('li');
        modelLegende.id = 'modelLegende';
        modelLegende.className = 'glide_slide';
    
    //creation de l'image du model legende
    var imgModelLegende = document.createElement('img');
        imgModelLegende.id = 'imgModelLegende';
        imgModelLegende.src = './assets/configurateur/modele/selection/Legende.png';
    
    //incersion des élément dans le dom
    document.getElementById('app').appendChild(config);
    document.getElementById('config').appendChild(carousel1);
    document.getElementById('carousel1').appendChild(visualisation);
    document.getElementById('visualisation').appendChild(choixVisualisation);
    document.getElementById('choixVisualisation').appendChild(modelPure);
    document.getElementById('modelPure').appendChild(imgModelPure);
    document.getElementById('choixVisualisation').appendChild(modelLegende);
    document.getElementById('modelLegende').appendChild(imgModelLegende);
}

config();

new Glide('.glide').mount();